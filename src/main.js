import Vue from 'vue'
import App from './App.vue'
import router from "./router";
import ModeChange from "./components//ModeChange.vue"
import "./assets/dark.css"; 
import "./assets/style.css"
import { library } from '@fortawesome/fontawesome-svg-core'
import {fas} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
library.add(fas);
Vue.component('font-awesome-icon', FontAwesomeIcon )
Vue.component('mode-change', ModeChange)
Vue.config.productionTip = false
new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
